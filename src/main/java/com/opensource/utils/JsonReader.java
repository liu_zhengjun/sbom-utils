package com.opensource.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.opensource.entity.PackageLicense;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JsonReader {
    public static String readFile(String filePath) throws IOException {
        StringBuilder content = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }
        bufferedReader.close();
        return content.toString();
    }

    /**
     * 读取json文件 获取合规信息
     * @param args
     */
    public static List<PackageLicense> getLincense() throws IOException {
        String json = readFile("D:\\sbom\\sbom-content\\Sbom-json\\openEuler-24.03-epol-X86_64.json");
        List<PackageLicense> packageLicenseList = new ArrayList<>();
        JSONObject jsonObject = JSON.parseObject(json);
        JSONArray jsonArray = JSON.parseArray(jsonObject.getString("packages"));
        System.out.println(jsonArray.size());
        HttpUtil httpUtil = new HttpUtil();
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        //获取信息 包名 SPDXID  license信息 licenseDeclared
        for (int i = 0; i < jsonArray.size(); i++) {
            PackageLicense packageLicense = new PackageLicense();
            JSONObject spdxObject = jsonArray.getJSONObject(i);
            String isCom;
            try {
                isCom = httpUtil.get(spdxObject.getString("licenseDeclared"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            packageLicense.setSpdxId(spdxObject.getString("name"));
            packageLicense.setLicense(spdxObject.getString("licenseDeclared"));
            packageLicense.setResult(isCom);
            packageLicenseList.add(packageLicense);
            System.out.println(spdxObject.getString("name") + spdxObject.getString("licenseDeclared") + isCom);
        }
        return packageLicenseList;
    }

    /**
     * 写入excel
     * @param args
     */
    public static void main(String[] args) throws IOException {
        List<PackageLicense> packageLicenseList = JsonReader.getLincense();

        //写入excel
        ExcelUtils excelUtils = new ExcelUtils();
        List<String> headList = new ArrayList<>();
        List<List<String>> contentList = new ArrayList<>();
        headList.add("包名");
        headList.add("license");
        headList.add("合规");

        //填充表格
        for (int i = 0; i < packageLicenseList.size(); i++) {
            List<String> content = new ArrayList<>();
            content.add(packageLicenseList.get(i).getSpdxId());
            content.add(packageLicenseList.get(i).getLicense());
            content.add(packageLicenseList.get(i).getResult());
            contentList.add(content);
        }
        excelUtils.setSheet("license");
        excelUtils.createHead(headList);
        excelUtils.createContent(contentList);
        excelUtils.writeToFile("D:\\license\\24.03-LTS-24.03-epol-X86_64-license.xlsx");
        System.out.println("license信息输出");

    }

}
