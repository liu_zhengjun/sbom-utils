package com.opensource.utils;

import com.opensource.entity.Issue;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ImportData {
    public static void readFileByLine(String filepath) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(filepath));
        String line = null;
        List<Issue> list = new ArrayList<>();
        while ((line = reader.readLine())!= null) {
            String[] chars = line.split(",");
            if (chars.length <= 5 || chars[0].charAt(0) != 'C') {
                continue;
            }
            Issue issue = new Issue();
            issue.setCveNumber(chars[0]);
            issue.setIssueId(chars[1]);
            issue.setIssueCreateTime(chars[2]);
            issue.setLevel(chars[3]);
            issue.setVulStatus(chars[4]);
            issue.setIssueStatus(chars[5]);
            list.add(issue);
        }
        reader.close();
        String insertLine;
        PrintWriter writer = new PrintWriter(new FileWriter("C:\\Users\\liuzhengjun\\sbom\\vul_insert_sql.txt"));
        for (Issue issue : list) {
            insertLine = "INSERT INTO public.package_vul_issue (cve_number, issue_id, issue_create_time, level, vul_status, issue_status, vul_awareness_duration, vul_patch_time, patch_release_time, cvss_score, nvd_score, software_name, complete_slo, issue_plan_complete_time, create_user, vul_release_time, vtopia_vul_release_time, sa_time, affect_branch, unaffected_branch, unanalyz_branch, organization_score, milestone, gitee_issue_url, issue_label, vtopia_recognition_time, affect_version, sig, team, charge_person) VALUES ('"+ issue.getCveNumber() + "', '" + issue.getIssueId() +"', '" + issue.getIssueCreateTime() + "', '" + issue.getLevel() +"', '" + issue.getVulStatus() + "', '" + issue.getIssueStatus() +"', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);";
            writer.println(insertLine);
        }
        writer.close();

    }

    public static void main(String[] args) throws Exception {
        readFileByLine("C:\\Users\\liuzhengjun\\sbom\\漏洞状态.csv");
    }

}
