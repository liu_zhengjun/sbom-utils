package com.opensource.utils;

import org.slf4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;



public class CompressToBase64 {
    public static void main(String[] args) {
        String filePath = "C:\\Users\\liuzhengjun\\sbom\\datapackage\\signatrust_definition_file.tar.gz";
        String base64String = compressToBase64(filePath);
        System.out.println(base64String);
    }
    public static String compressToBase64(String filePath) {
        try {
            File inputFile = new File(filePath);
            FileInputStream fileInputStream = new FileInputStream(inputFile);
            byte[] bytesArray = new byte[(int) inputFile.length()];
            fileInputStream.read(bytesArray);
            Base64.Encoder encoder = Base64.getEncoder();
            return encoder.encodeToString(bytesArray);
        } catch (IOException e) {

        }
        return "";
    }
}
