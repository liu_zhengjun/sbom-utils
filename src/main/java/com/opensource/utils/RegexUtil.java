package com.opensource.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
    public static void main(String[] args) {
        String regex = "^[a-zA-Z0-9-_]*$";

        String text = "AAABBB";

        Pattern pattern = Pattern.compile(regex);

        Matcher matcher = pattern.matcher(text);

        System.out.println(matcher.find());
    }
}
