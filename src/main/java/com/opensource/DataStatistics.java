package com.opensource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.opensource.utils.JsonReader;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataStatistics {
    public static void main(String[] args) throws IOException {
        String json = JsonReader.readFile("C:\\Users\\liuzhengjun\\sbom\\json\\sbom.txt");
        JSONArray jsonArray = JSON.parseArray(json);
        Map<String, Integer> result = new HashMap<>();
        int csum = 0, hsum = 0, msum = 0, lsum = 0, nsum = 0, usum = 0;
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int criticalVulCount = jsonObject.getJSONObject("statistics").getInteger("criticalVulCount");
            int highVulCount = jsonObject.getJSONObject("statistics").getInteger("highVulCount");
            int mediumVulCount = jsonObject.getJSONObject("statistics").getInteger("mediumVulCount");
            int lowVulCount = jsonObject.getJSONObject("statistics").getInteger("lowVulCount");
            int noneVulCount = jsonObject.getJSONObject("statistics").getInteger("noneVulCount");
            int unknownVulCount = jsonObject.getJSONObject("statistics").getInteger("unknownVulCount");
            csum += criticalVulCount;
            hsum += highVulCount;
            msum += mediumVulCount;
            lsum += lowVulCount;
            nsum += noneVulCount;
            usum += unknownVulCount;
        }
        System.out.println(csum);
        System.out.println(hsum);
        System.out.println(msum);
        System.out.println(lsum);
        System.out.println(nsum);
        System.out.println(usum);
        System.out.println(csum + hsum + msum + lsum + nsum + usum);
    }
}
