package com.opensource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.opensource.utils.JsonReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PackageStatistics {
    public static void main(String[] args) throws IOException {
        String json = JsonReader.readFile("C:\\Users\\liuzhengjun\\sbom\\json\\openEuler-20.03-LTS-SP4-ISO-source.iso-spdx-sbom.json");
        JSONArray jsonArray = JSON.parseArray(json);
        List<String> pkgList = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONArray jsonArrayExternalRefs = jsonObject.getJSONArray("externalRefs");
            for (int j = 0; j < jsonArrayExternalRefs.size(); j++) {
                pkgList.add(jsonArrayExternalRefs.getJSONObject(j).getString("referenceLocator"));
            }
        }

        File file = new File("C:\\Users\\liuzhengjun\\sbom\\json\\full_purl_list.txt");
        FileWriter writer = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        for (String pkg : pkgList) {
            bufferedWriter.write(pkg);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        System.out.println(pkgList.size());
    }
}
