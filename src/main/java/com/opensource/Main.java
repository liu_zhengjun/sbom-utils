package com.opensource;

import com.alibaba.fastjson2.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opensource.utils.JsonReader;


import java.io.IOException;

/**
 * 格式化json数据，转化为可发送请求的string
 */
public class Main {


    public static void main(String[] args) throws IOException {
        JsonReader jsonReader = new JsonReader();
        String content = JsonReader.readFile("D:\\sbom\\sbom-content\\Sbom-json\\openEuler-24.03-x86_64-everything.json");
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        String jsonStr = gson.toJson(content);
        System.out.println(jsonStr);
    }
}
