package com.opensource.entity;

import lombok.Data;

@Data
public class Issue {
    public String cveNumber;

    public String issueId;

    public String issueCreateTime;

    public String level;

    public String vulStatus;

    public String issueStatus;
}
