package com.opensource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbomUtilsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbomUtilsApplication.class, args);
	}

}
